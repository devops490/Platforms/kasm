# Self-Hosted Config

The repository houses all my configuration files and infrastructure files for my local self hosted applications

### Applications

|       Utility       | Application |
| :------------------: | ----------- |
| Desktop as a Service | KASM        |
|                      |             |
