KASM_VERSION="1.10.0.238225"
KASM_URL="https://kasm-static-content.s3.amazonaws.com/kasm_release_$KASM_VERSION.tar.gz"
SERVICE_IMAGES="https://kasm-static-content.s3.amazonaws.com/kasm_release_service_images_amd64_$KASM_VERSION.tar.gz"
WORKSPACE_IMAGES="https://kasm-static-content.s3.amazonaws.com/kasm_release_workspace_images_amd64_$KASM_VERSION.tar.gz"

# OPERATION=$1

# if [ -z "$OPERATION" ]; then
#     echo "No argument supplied"
#     exit
# fi

# WSL hack
if [ -d ".vagrant" ]; then
    mv .vagrant ~/.kasm
    ln -sv ~/.kasm .vagrant
else
    rm -rf ~/.kasm
    mkdir -p ~/.kasm
    ln -sv ~/.kasm .vagrant
fi

DIR="/mnt/c/Workspace/KASM/images"
[ ! -d "$DIR" ] && mkdir -p "$DIR" && wget $KASM_URL -P $DIR -O $DIR/kasm.tar.gz && wget $SERVICE_IMAGES -P $DIR -O $DIR/service_images.tar.gz && wget $WORKSPACE_IMAGES -P $DIR -O $DIR/workspace_images.tar.gz

# echo $operation

# status=$( vagrant status kasm --machine-readable | grep kasm,state, | awk  -F',' '{ print $4 }' )
# if [ $status != "running" ]; then 
    vagrant up 
    
# else
#     vagrant halt -f
# fi